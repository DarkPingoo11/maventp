package fr.tristan.projet;

import java.sql.*;
import java.util.List;

/**
 * @author Tristan LE GACQUE
 * Created 21/11/2018
 */
public class DBConnector {

    private static final String TABLE = "ville_france";


    public DBConnector() {
    }

    private Connection getConnection(){
        Information information = Information.getInstance();
        Connection con = null;
        try {
            con = DriverManager.getConnection(
                    information.getJdbc(),information.getUser(),information.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return con;
    }

    public void saveOnDatabase(List<LaposteObject> objects) {
        Connection con = this.getConnection();

        //If table not exist, create
        if(!this.checkTableExist(con)) {
            this.createTable(con);
        }

        String queryI = "INSERT INTO " + TABLE + " VALUES(?, ?, ?, ?, ?, ?, ?)";
        String queryU = "UPDATE " + TABLE + " SET Nom_commune=?, Code_postal=?, Libelle_acheminement=?, Ligne_5=?, Latitude=?, Longitude=? WHERE Code_commune_INSEE=?";
        try (PreparedStatement stI = con.prepareStatement(queryI); PreparedStatement stU = con.prepareStatement(queryU)){
            int requestsU = 0;
            int requestsI = 0;
            int max = objects.size();
            int current = 0;

            int pState = 0;
            int state = 0;
            for(LaposteObject obj : objects) {
                current++;
                state = current * 100 / max;

                if(state != pState) {
                    pState = state;
                    System.out.println("Insertion des entitées... " +state + "%");

                }
                if(this.existInDatabase(con, obj)) {
                    stU.setString(1, obj.getNomCommune());
                    stU.setString(2, obj.getCodePostal());
                    stU.setString(3, obj.getLibelle());
                    stU.setString(4, obj.getLigne5());
                    stU.setString(5, obj.getLatitude());
                    stU.setString(6, obj.getLongitude());
                    stU.setString(7, obj.getCodeCommune());

                    stU.execute();
                    requestsU++;

                } else {
                    stI.setString(1, obj.getCodeCommune());
                    stI.setString(2, obj.getNomCommune());
                    stI.setString(3, obj.getCodePostal());
                    stI.setString(4, obj.getLibelle());
                    stI.setString(5, obj.getLigne5());
                    stI.setString(6, obj.getLatitude());
                    stI.setString(7, obj.getLongitude());

                    stI.execute();
                    requestsI++;
                }
            }

            System.out.println("Insertion de " + requestsI + " entrées");
            System.out.println("Mise à jour de " + requestsU + " entrées");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean existInDatabase(Connection connection, LaposteObject obj) {
        String query = "SELECT * FROM " + TABLE + " WHERE Code_commune_INSEE = ?";
        boolean exist = false;

        try(PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, obj.getCodeCommune());
            final ResultSet resultSet = ps.executeQuery();

            exist = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return exist;
    }

    public boolean checkTableExist(Connection connection) {
        boolean exist = true;
        try(Statement st = connection.createStatement()) {
            st.executeQuery("SELECT * FROM " + TABLE + " LIMIT 1");
        } catch (SQLException e) {
            exist = false;
        }

        return exist;
    }

    public void createTable(Connection connection) {
        String query = "create table " + TABLE + " " +
                "( " +
                "  Code_commune_INSEE   varchar(255) not null " +
                "    primary key, " +
                "  Nom_commune          varchar(255) not null, " +
                "  Code_postal          varchar(255) not null, " +
                "  Libelle_acheminement varchar(255) not null, " +
                "  Ligne_5              varchar(255) not null, " +
                "  Latitude             varchar(255) not null, " +
                "  Longitude            varchar(255) not null " +
                ") " +
                "  engine = InnoDB;";

        try (Statement stmt = connection.createStatement()){
            stmt.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

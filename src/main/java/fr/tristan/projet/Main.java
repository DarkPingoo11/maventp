package fr.tristan.projet;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tristan LE GACQUE
 * Created 21/11/2018
 */
public class Main {

    public static void main(String[] args) {
        Information information = Information.getInstance();
        System.out.println("========= MAVEN TP ========= ");
        System.out.println("VERSION : " + information.getVersion());
        System.out.println(" ");


        if(args.length == 1) {
            final List<LaposteObject> cities = readCSV(args[0]);
            System.out.println("Récupération de " + cities.size() + " objets");

            DBConnector db = new DBConnector();
            db.saveOnDatabase(cities);
        } else {
            System.out.println("Merci d'indiquer le chemin vers le fichier à lire en unique paramètre !");
            System.exit(0);
        }
    }

    private static List<LaposteObject> readCSV(String filename) {
        List<LaposteObject> objects = new ArrayList<>();
        try(Reader reader = Files.newBufferedReader(Paths.get(filename))) {
            final CSVParser parser = new CSVParserBuilder().withSeparator(';')
                            .withIgnoreQuotations(true)
                            .build();
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();

            String[] record;
            while((record = csvReader.readNext()) != null) {
                objects.add(new LaposteObject(record));
            }
        } catch (IOException e) {
            e.printStackTrace();
            exitWithError("Le chemin d'accès est introuvable");
        }

        return objects;
    }

    private static void exitWithError(String error) {
        System.out.println("ERREUR : " + error);
        System.exit(1);
    }




}

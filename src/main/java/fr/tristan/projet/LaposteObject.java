package fr.tristan.projet;

/**
 * @author Tristan LE GACQUE
 * Created 21/11/2018
 */
public class LaposteObject {

    private String codeCommune;
    private String nomCommune;
    private String codePostal;
    private String libelle;
    private String ligne5;
    private String gps;

    public LaposteObject(String[] object) {
        this.codeCommune = object[0];
        this.nomCommune = object[1];
        this.codePostal = object[2];
        this.libelle = object[3];
        this.ligne5 = object[4];
        this.gps = object[5];
    }

    public String getCodeCommune() {
        return codeCommune;
    }

    public String getNomCommune() {
        return nomCommune;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getLigne5() {
        return ligne5;
    }

    public String getLatitude() {
        return gps == null || gps.split(",").length != 2 ? "0" : gps.split(",")[0].replaceAll(" ", "");
    }

    public String getLongitude() {
        return gps == null || gps.split(",").length != 2 ? "0" : gps.split(",")[1].replaceAll(" ", "");
    }
}

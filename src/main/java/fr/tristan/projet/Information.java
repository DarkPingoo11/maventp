package fr.tristan.projet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Tristan LE GACQUE
 * Created 14/11/2018
 */
public class Information {
    private static final String FILE = "/info.properties";
    private static Information ourInstance = new Information();

    public static Information getInstance() {
        return ourInstance;
    }

    private final String version;
    private final String os;
    private final String jdbc;
    private final String user;
    private final String password;

    private Information() {
        Properties prop = new Properties();

        try (InputStream is = getClass().getResourceAsStream(FILE)) {
            prop.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.version = prop.getProperty("version");
        this.os = prop.getProperty("os");
        this.jdbc = prop.getProperty("jdbc");
        this.user = prop.getProperty("user");
        this.password = prop.getProperty("pass");
    }

    public String getVersion() {
        return version;
    }

    public String getOs() {
        return os;
    }

    public String getJdbc() {
        return jdbc;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
